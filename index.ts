import _ = require('lodash')
import repl = require('repl')

// set context, used by this scene
// use an completer which parse methods & commands from the scene.
// set commands, override the .load.
// customize completer and writer.

export type IEvalCallback = (err?: Error | null, ret?: any) => void

export type IMethod = (this: repl.REPLServer, args: string[], cb: IEvalCallback) => void

export interface ICommand {
  help: string,
  action: (this: repl.REPLServer) => void
}

export interface IScene {
  prompt?: string
  context?: any
  methods?: {[index: string]: IMethod}
  commands?: {[index: string]: ICommand}
}

export function startRepl(defaultScene: IScene) {

  let that: any = repl.start({
    prompt: defaultScene.prompt || "$ ",
    eval: myEval,
    writer: _.identity,
    completer: myCompleter,
  })

  // set default context
  that.context = {}
  that.methods = {}
  // save default commands
  let defaultCommands = that.commands

  setScene(defaultScene)

  // functions

  function myEval(cmdString: string, context, filename, callback) {
    let str = cmdString.trim()
    if (str.length > 0) {
      let [cmd, ...args] = str.split(/\s+/)
      let functor = that.methods[cmd].call(that, args, callback)
    }
    else {
      callback()
    }
  }

  function myWriter(obj) {
    return obj
  }

  function myCompleter(currentSubString) {
    let methods = _.keys(that.methods)
    let commands = _.keys(that.commands).map(k => '.' + k)
    let completions = _.concat(methods, commands)
    let hits = _.filter(completions, (c) => { return c.indexOf(currentSubString) == 0 })
    // show all completions if none found
    return [hits.length ? hits : completions, currentSubString];
  }

  function setScene(scene: IScene) {
    scene.prompt && setPrompt(scene.prompt)
    scene.context && setContext(scene.context)
    scene.methods && setMethods(scene.methods)
    scene.commands && setCommands(scene.commands)
  }

  function setPrompt(prompt: string) { that._initialPrompt = prompt }

  function setContext(context: any) { that.context = context }

  function setMethods(methods: any) { that.methods = methods }

  function setCommands(commands: any) {
    that.commands = _.assign({}, defaultCommands, commands)
  }

  that.handler = {
    setScene, setPrompt, setContext, setMethods, setCommands,
  }

  return that as repl.REPLServer
}
