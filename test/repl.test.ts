import { startRepl } from '../index'

let repl = startRepl({
  // TODO: use data bind to set this prop
  prompt: "(^_^): ",
  context: { foo: 1 },
  methods: {
    a: function(args, cb) {
      cb(null, 'a method: ' + args)
    },
    b: function(args, cb) {
      cb(null, this.context)
    }
  },
  commands: {
    a: {
      help: "temp use a",
      action: function() {
        console.log('a ====================================');
        this.displayPrompt()
      }
    }
  }
})
