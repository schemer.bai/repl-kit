# use Scene to sse the repl envirement

```ts
export type IEvalCallback = (err?: Error | null, ret?: any) => void

export type IMethod = (this: repl.REPLServer, args: string[], cb: IEvalCallback) => void

export interface ICommand {
  help: string,
  action: (this: repl.REPLServer) => void
}

export interface IScene {
  prompt?: string
  context?: any
  methods?: {[index: string]: IMethod}
  commands?: {[index: string]: ICommand}
}
```