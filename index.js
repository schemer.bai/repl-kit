"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _ = require("lodash");
const repl = require("repl");
function startRepl(defaultScene) {
    let that = repl.start({
        prompt: defaultScene.prompt || "$ ",
        eval: myEval,
        writer: _.identity,
        completer: myCompleter,
    });
    // set default context
    that.context = {};
    that.methods = {};
    // save default commands
    let defaultCommands = that.commands;
    setScene(defaultScene);
    // functions
    function myEval(cmdString, context, filename, callback) {
        let str = cmdString.trim();
        if (str.length > 0) {
            let [cmd, ...args] = str.split(/\s+/);
            let functor = that.methods[cmd].call(that, args, callback);
        }
        else {
            callback();
        }
    }
    function myWriter(obj) {
        return obj;
    }
    function myCompleter(currentSubString) {
        let methods = _.keys(that.methods);
        let commands = _.keys(that.commands).map(k => '.' + k);
        let completions = _.concat(methods, commands);
        let hits = _.filter(completions, (c) => { return c.indexOf(currentSubString) == 0; });
        // show all completions if none found
        return [hits.length ? hits : completions, currentSubString];
    }
    function setScene(scene) {
        scene.prompt && setPrompt(scene.prompt);
        scene.context && setContext(scene.context);
        scene.methods && setMethods(scene.methods);
        scene.commands && setCommands(scene.commands);
    }
    function setPrompt(prompt) { that._initialPrompt = prompt; }
    function setContext(context) { that.context = context; }
    function setMethods(methods) { that.methods = methods; }
    function setCommands(commands) {
        that.commands = _.assign({}, defaultCommands, commands);
    }
    that.handler = {
        setScene, setPrompt, setContext, setMethods, setCommands,
    };
    return that;
}
exports.startRepl = startRepl;
