/// <reference types="node" />
import repl = require('repl');
export declare type IEvalCallback = (err?: Error | null, ret?: any) => void;
export declare type IMethod = (this: repl.REPLServer, args: string[], cb: IEvalCallback) => void;
export interface ICommand {
    help: string;
    action: (this: repl.REPLServer) => void;
}
export interface IScene {
    prompt?: string;
    context?: any;
    methods?: {
        [index: string]: IMethod;
    };
    commands?: {
        [index: string]: ICommand;
    };
}
export declare function startRepl(defaultScene: IScene): any;
